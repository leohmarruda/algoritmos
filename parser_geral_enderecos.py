import re
import unidecode

addresses = [
"Avenida Professor Manoel José Pedroso, 1347. Jardim Nomura - Cotia - SP. CEP 06717-900.",
"Av. Ver. Narciso Yague Guimarães, 277 - Centro Cívico - 08780-900.",
"Praça dos Três Poderes, 73 - Centro - Jacareí/SP - CEP 12327-170",
"Rua Luiz Passos Júnior, 50 - Centro - 11660-900.",
"Rua Dona Maria Alves, nº. 865, Centro, Ubatuba-SP, CEP 11680-000.",
"Rua Augusto Xavier de Lima, 251, bairro Jardim Jalisco, Resende/RJ, CEP: 27510-090",
" Rua Júlio Paulo Marcellini, nº 50 | CEP: 37018-050. ",
"Rua São Bento (rua 3), nº 840 – Centro",
"Praça Juscelino Kubitscheck, s/n. Mariana/MG | CEP 35420-000. ",
"Praça Júlio de Castilhos, s/n | CEP: 94410-055.",
    "Rua: Bahia, nº 40 - Centro | CEP: 17501-900. ",
    """Av. Engenheiro Eusébio Stevaux, 823
São Paulo - SP- CEP 04696-000 """,
    """Rua Prof. João da Matta e Luz, 84
CEP: 06401-120 - Centro - Barueri - SP""",
    "AVENIDA AUGUSTO FREIRE LOUTO, Nº - - JUSTIÇA II - ANCHIETA - ES - CEP: 29230-000",
    "RUA DOS MATUPIRIS, Nº 162 - - PRAIA DE GUANABARA - ANCHIETA - ES - CEP: 29230-000",
    """RUA JOSE ALVES DA COSTA, CENTRO, Nº 56, ARACRUZ-ESPIRITO SANTO
CEP.: 29190-010""",
    "Rua Bla, 1, ap 333, Bairro legal, São PAULO - SP, CEP 01234-000",
    "RUA ADAMASTOR SALVADOR, CENTRO, 277 4º ANDAR - 402 CEP: 29700-050 COLATINA, ESPIRITO SANTO - ES"
]

def remove_accents(accented_str):
    return unidecode.unidecode(accented_str)

def clean_str(s, trim=True, default="", len=None, blanks=['\t', '\n', '\r'], nulls=[]):
    """Clean a string for saving or database insertion.
    Remove invalid characters and trim.
    :params:
        default: value returned if string is empty or None
        blanks: characters to be replaced by single spaces.
        nulls: characters to be replaced by nulls (removed).
    """
    NULLS = ['\uf0fc', '\x83']
    BLANKS = ['\xa0', '\x93', '\x94']
    DASHES = ['\u221e', '\x96']
    UNDERLINES = ['\u2116', '\ufffd']
    if not s: return default
    if not isinstance(s, str): return s
    # Replace invalid characters
    clean_str = s
    for c in NULLS:
        clean_str = clean_str.replace(c, "")
    for c in BLANKS:
        clean_str = clean_str.replace(c, " ")
    for c in UNDERLINES:
        clean_str = clean_str.replace(c, "_")
    for c in DASHES:
        clean_str = clean_str.replace(c, "-")
    for c in blanks:
        clean_str = clean_str.replace(c, " ")
    for c in nulls:
        clean_str = clean_str.replace(c, "")
    if trim: clean_str = clean_str.strip()
    if len: return clean_str[:len]
    return clean_str


UF_estado = {
    'AC': 'Acre',
    'AL': 'Alagoas',
    'AP': 'Amapá',
    'AM': 'Amazonas',
    'BA': 'Bahia',
    'CE': 'Ceará',
    'DF': 'Distrito Federal',
    'ES': 'Espírito Santo',
    'GO': 'Goiás',
    'MA': 'Maranhão',
    'MT': 'Mato Grosso',
    'MS': 'Mato Grosso do Sul',
    'MG': 'Minas Gerais',
    'PA': 'Pará',
    'PB': 'Paraíba',
    'PR': 'Paraná',
    'PE': 'Pernambuco',
    'PI': 'Piauí',
    'RJ': 'Rio de Janeiro',
    'RN': 'Rio Grande do Norte',
    'RS': 'Rio Grande do Sul',
    'RO': 'Rondônia',
    'RR': 'Roraima',
    'SC': 'Santa Catarina',
    'SP': 'São Paulo',
    'SE': 'Sergipe',
    'TO': 'Tocantins'
}

def validate_UF(uf):
    for UF in UF_estado.keys():
        if uf.upper() == UF:
            return UF
    return False

def validate_estado(estado):
    for estado_ in UF_estado.values():
        if remove_accents(estado_.upper()) == remove_accents(estado.upper()):
            return estado_
    return False

def estado_to_UF(estado):
    estado = remove_accents(estado.upper())
    for UF, est in UF_estado.items():
        if remove_accents(est.upper()) == estado:
            return UF
    else:
        return False

def UF_to_estado(UF):
    for uf, estado in UF_estado.items():
        if uf.upper() == UF.upper():
            return estado
    else:
        return False

def parser_universal_de_enderecos(endereco):
    """
    Hipóteses:
        logradouro é sempre o primeiro item
        estado / UF está sempre depois do município, municipio depois do bairro
        todos ',', ' -', '- ' e '|' são separadores
        número é o primeiro valor que começa com um dígito numérico, ou com nº ou s/n
        se tem estado, procura municipio, se tem municipio procura bairro antes de complemento, do fim para o começo
        se não tem estado, procura bairro de trás pra frente
        procura complemento é o primeiro valor que sobrar depois do número
          bairro é o primeiro valor que não começa com dígito numérico que sobrar
          município é o primeiro valor que não começa com dígito numérico que sobrar depois de bairro
        outros entra dentro de complemento no final
    """
    parsed = {
        "logradouro": None,
        "número": None,
        "complemento": None,
        "bairro": None,
        "CEP": None,
        "município": None,
        "estado": None,
        "UF": None,
        "outros": None
    }
    if not endereco: return parsed

    # Extrai CEP
    cep_search = re.search("(?<= )\d{5}-?\d{3}(?= |\.|$)", endereco)
    if cep_search:
        cep = cep_search[0]
        endereco = endereco.replace(cep, ",")
    parsed['CEP'] = clean_str(cep.replace("-", "")) if cep_search else None
    endereco = re.sub("(C|c)(EP|ep).?:?", "", endereco)
    # Substitui s/n
    endereco = re.sub('(s|S)/(n|N)\.?', "Nº ,", endereco)

    # Procura especificamente por valores do tipo str-ESTADO ou str-UF
    for estado, UF in UF_estado.items():
        search_pattern = f"(?<=[A-Za-z])(-|–)(?={estado}|{estado.upper()}|{remove_accents(estado.upper())}|{UF})"
        estado_search = re.search(search_pattern, endereco)
        if estado_search:
            endereco = re.sub(search_pattern, ',' ,endereco)
            break

    # Quebra o resto em valores
    endereco = re.sub('(?<=\d)\. ', ",", endereco)  # Substitui pontos depois de números
    endereco = re.sub('( (-|–)|(-|–) )', ",", endereco)  # Substitui hífens com espaço
    endereco = endereco.replace("\n", ",").replace("|", ",").replace("/", ",").split(",")
    endereco = [clean_str(value) for value in endereco]
    # print(endereco)

    # Procura UF e estado, municipio e bairro do final para o começo
    for idx in range(len(endereco)-1):
        value = clean_str(endereco[-idx-1], nulls=['.'])
        if not parsed['UF'] and validate_UF(value):
            parsed['UF'] = validate_UF(value)
            parsed['estado'] = UF_to_estado(parsed['UF'])
            endereco[-idx-1] = None
        elif not parsed['UF'] and validate_estado(value):
            parsed['estado'] = validate_estado(value)
            parsed['UF'] = estado_to_UF(parsed['estado'])
            endereco[-idx-1] = None
        elif parsed['UF'] and value and value[0].isalpha() and not re.search('^((?:n|N)º?\.?\s*)(?=[^a-zA-Z]|$)', value):
            if not parsed['município']:
                parsed['município'] = value
                endereco[-idx-1] = None
            elif not parsed['bairro']:
                parsed['bairro'] = value
                endereco[-idx-1] = None
    for idx in range(len(endereco)-1):  # Se não tiver UF, mas tiver um valor alfabético presume que seja bairro
        value = clean_str(endereco[-idx-1], nulls=['.'])
        if not parsed['bairro'] and value and value[0].isalpha() and not re.search('^((?:n|N)º?\.?\s*)(?=[^a-zA-Z]|$)', value):
            parsed['bairro'] = value
            endereco[-idx-1] = None

    # Extrai logradouro
    if endereco[0]:
        parsed['logradouro'] = clean_str(endereco[0])
        endereco[0] = None

    idx_numero = None
    for idx, value in enumerate(endereco):
        if value and not parsed['número'] and value[0].isdigit():
            words = value.split(" ")
            parsed['número'] = words[0]
            parsed['número'] = re.sub('(\.|,)$', "", parsed['número'])
            if len(words) > 1: parsed['complemento'] = " ".join(words[1:])
            endereco[idx] = None
            idx_numero = idx
        elif value and not parsed['número'] and re.search('^((?:n|N)º?\.?\s*)(?=[^a-zA-Z]|$)', value):
            parsed['número'] = clean_str(re.sub('^((?:n|N)º?\.?\s*)(?=[^a-zA-Z]|$)', "", value))
            words = parsed['número'].split(" ")
            parsed['número'] = words[0] or None
            if parsed['número']: parsed['número'] = re.sub('(\.|,)$', "", parsed['número'])
            if len(words) > 1: parsed['complemento'] = " ".join(words[1:])
            endereco[idx] = None
            idx_numero = idx
        else:
            if value is not None and not parsed['complemento'] and idx_numero == idx - 1:
                parsed['complemento'] = clean_str(value, nulls=['.']) or None
                endereco[idx] = None
            elif value and value[0].isalpha():
                if not parsed['bairro']:
                    parsed['bairro'] = value
                    endereco[idx] = None
                elif not parsed['município']:
                    parsed['município'] = value
                    endereco[idx] = None

    if parsed['bairro']: parsed['bairro'] =  re.sub('(B|b)airro ', "", parsed['bairro'])
    # Adiciona campos remanescentes em complemento
    parsed['outros'] = [value for value in endereco if clean_str(value, nulls=['.'])] or None
    if parsed['outros']: parsed['complemento'] = (parsed['complemento'] + ", " if parsed['complemento'] else "") + ", ".join(parsed['outros'])
    parsed.pop('outros')
    return parsed

for address in addresses:
    print(address)
    print(parser_universal_de_enderecos(address))